# Gitlab CI/CD Pipeline for Code Test and Code Analysis of a Sample PHP Project 

### Includes
+ PhpUnit tests/code coverage
+ SonarCloud Code analysis

# Configure this project
Fork this repository in gitlab

# Configure your Sonar Cloud Project Details 

+ Setup Project and Organization from Sonar Cloud
```
sonar.projectKey=Dipendu_world4help
sonar.organization=dipendu
```

+ Setup variables for SonarCloud
Make sure the variables are not protected, so they are available in all the branches
```
SONAR_HOST_URL
SONAR_TOKEN
```

# Congrats
You can view the results 
+ Test results in the pipeline
+ Code analysis results on [sonarcloud.io](#https://sonarcloud.io/) 

### References
+ [Php Unit](#https://phpunit.readthedocs.io/)
+ [Sonarcloud - Gitlab integration](#https://sonarcloud.io/documentation/integrations/ci/gitlab-ci/)


