<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class IndexTest extends TestCase
{
    public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $this->assertInstanceOf(
            Index::class,
            Index::fromString('user@example.com')
            );
    }
    
    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);
        
        Index::fromString('invalid');
    }
    
    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(
            'user@example.com',
            Index::fromString('user@example.com')
            );
    }
}
