<?php declare(strict_types=1);
final class Index
{
    private $email;
    
    private function __construct(string $email)
    {
        $this->ensureIsValidEmail($email);
        
        $this->email = $email;
        
        $this->add(1,2,3,4);
    }
    
    public static function fromString(string $email): self
    {
        return new self($email);
    }
    
    public function __toString(): string
    {
        return $this->email;
    }
    
    private function ensureIsValidEmail(string $email): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a valid email address',
                    $email
                    )
                );
        }
    }
    
    function add($a,$b,$c,$d) {
        return $a+$b;
    }
    
    /*function sub($a,$b,$c) {
     return $a-$b;
     }*/
    
    /*echo $d;
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";
     echo "dipendu roy";*/
}